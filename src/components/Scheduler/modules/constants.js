export const WEEKS = {
    1: {
        short: 'Пон',
        full: 'Понедельник',
        en: 'Mon'
    },
    2: {
        short: 'Вт',
        full: 'Среда',
        en: 'Tue'
    },
    3: {
        short: 'Ср',
        full: 'Среда',
        en: 'Wed'
    },
    4: {
        short: 'Чт',
        full: 'Четверг',
        en: 'Thu'
    },
    5: {
        short: 'Пт',
        full: 'Пятница',
        en: 'Fri'
    },
    6: {
        short: 'Сб',
        full: 'Суббота',
        en: 'Sat'
    },
    7: {
        short: 'Вс',
        full: 'Воскресенье',
        en: 'Sun'
    }
}

export const MONTH_TYPES = {
    PREV: 'PREV',
    CURRENT: 'CURRENT',
    NEXT: 'NEXT'
}