import moment from 'moment'

const addTime = (firsTime, secondTime) => {
    let hour = parseInt(firsTime[0]) + parseInt(secondTime[0])
    let minutes = parseInt(firsTime[1]) + parseInt(secondTime[1])
    if (minutes > 59) {
        hour++
        minutes = minutes - 60
    }
    return `${hour}:${minutes || '00'}`
}

export const getDateFromString = (date) => new Date(moment(date, 'DD.MM.YYYY').format("YYYY-MM-DD HH:mm:ss"))

export const getDayFromString = (date) => parseInt(moment(date, 'DD.MM.YYYY').format('D'))

export const getMonthFromString = (date) => parseInt(moment(date, 'DD.MM.YYYY').format('M'))

export const getYearFromString = (date) => parseInt(moment(date, 'DD.MM.YYYY').format('YYYY'))

export const daysInMonth = (month, year) => new Date(year, month, 0).getDate()

export const calculateWeekDayOfMonth = (day, month, year) => new Date(year, month - 1, day).getDay() || 7

export const calculateFirstWeekDayOfMonth = (month, year) => new Date(year, month - 1, 1).getDay() || 7

export const getDay2Digit = (day) => day > 9 ? day : `0${day}`

export const getMonth2Digit = (month) => month > 9 ? month : `0${month}`

export const getDay = (i) => ++i

export const generateWorkTime = (startTime, endTime) => `с ${startTime} до ${endTime}`

export const generateReservationTime = (reservationDate, duration) => { //reservationTime = 28.02.2020 17:55, duration: 1:15
    const dateTime = reservationDate.split(' ') // ['28.02.2020', '17:55']
    const hourMinTime = dateTime[1].split(':') // ['17', '55']
    const durationTime = duration.split(':')// ['1', '15']
    const startTime = dateTime[1]
    const endTime = addTime(hourMinTime, durationTime)
    return `${startTime} до ${endTime}`
}