import firebase from 'firebase/app'
import 'firebase/firestore';
import 'firebase/messaging';
import 'firebase/storage';

const config = {
    apiKey: "AIzaSyAE0C5omlWMgf4aBJB3qKqygp6UhNTkzQs",
    authDomain: "checkin-notification-260ed.firebaseapp.com",
    databaseURL: "https://checkin-notification-260ed.firebaseio.com",
    projectId: "checkin-notification-260ed",
    storageBucket: "checkin-notification-260ed.appspot.com",
    messagingSenderId: "114594238374",
    appId: "1:114594238374:web:8b180f06f96a1a2fac0967",
    measurementId: "G-QEZB3C5Z27"
  };
firebase.initializeApp(config)

// Initialize Cloud Firestore through Firebase
let db = firebase.firestore();

// Disable deprecated features
// db.settings({
//   timestampsInSnapshots: true
// });

// db.enablePersistence({experimentalTabSynchronization:true})

const storage = firebase.storage()

const messaging = firebase.messaging()

export default {
  db,
  storage,
  messaging
}