import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import { v1 as uuid } from "uuid";
import http from "@/methods/http";
import { MONTH_TYPES } from "@/components/Scheduler/modules/constants";
import { config } from "@/config";

const { FIREBASE } = config;

Vue.use(Vuex);

const getNotificationsFromStorage = () => {
  const storageNotifications = localStorage.getItem("notifications");
  if (!storageNotifications) return [];
  let notifications = [];
  try {
    notifications = JSON.parse(storageNotifications);
  } catch (err) {
    console.log({ err });
    notifications = [];
  }
  return notifications;
};

const setNotificationsIntoStorage = (notifications) => {
  localStorage.setItem("notifications", JSON.stringify(notifications));
};

export default new Vuex.Store({
  state: {
    sliderInHeaderShown: false,
    authModal: {
      state: false,
      tab: "auth",
    },
    authenticated: false,
    token: "",
    profile: {},
    notification: { type: "info", text: "" },
    showMainMenu: false,

    //for calendar order changes
    orderForm: false,
    timeForm: false,
    printForm: false,
    bufferForm: false,
    copyBuffer: false,
    cutBuffer: false,

    //calendar
    schedule: [],
    prevMonthSchedule: [],
    nextMonthSchedule: [],
    reservations: [],
    prevMonthReservations: [],
    nextMonthReservations: [],

    //notifications
    notifications: [],

    //firebase
    clientToken: "",
    isClientInit: false,
  },
  getters: {
    mainMenu: function(state) {
      return state.showMainMenu;
    },
    authState: function(state) {
      return state.authenticated;
    },
    getFirebaseIsInit() {
      const token = localStorage.getItem("FIREBASE_is_init");
      return token !== null;
    },
  },
  mutations: {
    toggleMainMenu: function(state) {
      state["showMainMenu"] = !state["showMainMenu"];
    },
    closeMainMenu: function(state) {
      state["showMainMenu"] = false;
    },
    closeEverything: function(state) {
      state["showMainMenu"] = false;
      state["showCart"] = false;
    },
    showSliderInHeader(state) {
      state.sliderInHeaderShown = true;
    },
    hideSliderInHeader(state) {
      state.sliderInHeaderShown = false;
    },
    showAuthModal(state) {
      state.authModal.state = true;
    },
    hideAuthModal(state) {
      state.authModal.state = false;
    },
    notificate: function(state, payload) {
      state["notification"] = payload;
    },
    setRegisterTab(state) {
      state.authModal.tab = "reg";
    },
    setAuthTab(state) {
      state.authModal.tab = "auth";
    },
    authenticate(state) {
      state.authenticated = true;
    },
    logout(state) {
      state.authenticated = false;
      state.profile = { first_name: "", last_name: "" };
      state.token = "";
    },
    setToken(state, payload) {
      state.token = payload;
    },
    setProfile(state, payload) {
      state.profile = payload;
    },
    changeOrderForm(state, value) {
      state.orderForm = value;
    },
    changeTimeForm(state, value) {
      state.timeForm = value;
    },
    changePrintForm(state, value) {
      state.printForm = value;
    },
    changeBufferForm(state, value) {
      state.bufferForm = value;
    },
    changeCopyBuffer(state, value) {
      state.copyBuffer = value;
    },
    changeCutBuffer(state, value) {
      state.cutBuffer = value;
    },

    //scheduler
    setSchedule(state, schedule) {
      state.schedule = schedule;
    },
    setPrevSchedule(state, schedule) {
      state.prevMonthSchedule = schedule;
    },
    setNextSchedule(state, schedule) {
      state.nextMonthSchedule = schedule;
    },
    setReservations(state, schedule) {
      state.reservations = schedule;
    },
    setPrevReservations(state, schedule) {
      state.prevMonthReservations = schedule;
    },
    setNextReservations(state, schedule) {
      state.nextMonthReservations = schedule;
    },

    //notifications
    setNotifications(state, notifications) {
      state.notifications = notifications;
    },

    //firebase
    setClientToken(state, token) {
      state.clientToken = token;
    },
    setClientInit(state) {
      state.isClientInit = true;
    },
  },
  actions: {
    //scheduler
    async getScheduleByMonth(
      ctx,
      { month, year, spec_slug, daysInMonth, monthType }
    ) {
      const response = await http(
        `masters/${spec_slug}/schedule/beta`,
        "get",
        false,
        {
          day: `01.${month}.${year}`,
          day_count: daysInMonth,
        }
      );
      switch (monthType) {
        case MONTH_TYPES.CURRENT:
          ctx.commit("setSchedule", response.data);
          break;
        case MONTH_TYPES.PREV:
          ctx.commit("setPrevSchedule", response.data);
          break;
        case MONTH_TYPES.NEXT:
          ctx.commit("setNextSchedule", response.data);
          break;
        default:
          break;
      }
      return response.data;
    },

    async getReservationsByMonth(ctx, { month, year, spec_slug, monthType }) {
      const response = await http(
        `masters/${spec_slug}/reservations`,
        "get",
        false,
        {
          date_time_start: `01.${month}.${year}`,
          date_time_end: `01.${month + 1}.${year}`,
        }
      );

      switch (monthType) {
        case MONTH_TYPES.CURRENT:
          ctx.commit("setReservations", response.data);
          break;
        case MONTH_TYPES.PREV:
          ctx.commit("setPrevReservations", response.data);
          break;
        case MONTH_TYPES.NEXT:
          ctx.commit("setNextReservations", response.data);
          break;
        default:
          break;
      }

      return response.data;
    },

    //firebase
    async initToken(ctx, { userId, token }) {
      const headers = {
        Authorization: FIREBASE.SERVER_KEY,
        "Content-Type": "application/json",
      };
      const body = {
        id: userId,
        token,
      };
      try {
        const res = await axios.post(FIREBASE.ADD_TOKEN_URL, body, { headers });
        if (res.status === 200) {
          localStorage.setItem("FIREBASE_client_token", token);
          localStorage.setItem("FIREBASE_is_init", "set");
          ctx.commit("setClientToken", token);
          ctx.commit("setClientInit", true);
        }
      } catch (err) {
        console.log({ store: true, err });
      }
    },

    initNotifications(ctx, token) {
      const notifications = getNotificationsFromStorage();
      const userNotifications = notifications.filter(
        (notif) => notif.token === token
      );
      ctx.commit("setNotifications", userNotifications);
    },
    addNotification(ctx, { notification, token }) {
      const notifications = getNotificationsFromStorage();
      notification.id = uuid();
      notification.isRead = false;
      notification.token = token;
      notifications.push(notification);
      setNotificationsIntoStorage(notifications);
      ctx.commit("setNotifications", notifications);
    },
    readNotification(ctx, id) {
      const notifications = getNotificationsFromStorage();
      const filteredNotifications = notifications.filter(
        (notif) => notif.id !== id
      );
      setNotificationsIntoStorage(filteredNotifications);
      ctx.commit("setNotifications", filteredNotifications);
    },
  },
});
