import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import fontawesome from "@fortawesome/fontawesome";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import lightawesome from "@fortawesome/fontawesome-pro-light";
import solidawesome from "@fortawesome/fontawesome-pro-solid";
import regawesome from "@fortawesome/fontawesome-pro-regular";
import brandsawesome from "@fortawesome/fontawesome-free-brands";
import VueCookie from "vue-cookie";
import * as VueGoogleMaps from "vue2-google-maps";
import VueTelInput from "vue-tel-input";
import VTooltip from "v-tooltip";
import "./registerServiceWorker";
import "vue-tel-input/dist/vue-tel-input.css";

fontawesome.library.add(lightawesome);
fontawesome.library.add(solidawesome);
fontawesome.library.add(regawesome);
fontawesome.library.add(brandsawesome);

Vue.component("font-awesome-icon", FontAwesomeIcon);
Vue.use(VueCookie);
Vue.use(VueTelInput);
Vue.use(VTooltip);

Vue.use(VueGoogleMaps, {
  load: {
    // old key AIzaSyBDbGP2PzVxQw0o-CuiJxOkT5b3tx4TNrI
    //  disable AIzaSyCJxqd0tqwC7wlpe7ZEcLcokSelS1FlkBg

    key: "AIzaSyAxJI0D5zIOVFrKBeK0RIK7egjRe4QGO_o",
    libraries: "places",
    installComponents: true,
  },
});

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");

// export function createApp() {
//   const app = new Vue({
//     router,
//     store,
//     render: h => h(App)
//   });

//   return { app, router, store };
// }
