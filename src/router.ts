import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);

export default new Router({
  mode: 'history',
  scrollBehavior(to, from, savedPosition) {
    return { x: 0, y: 0 }
  },
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('./views/Home.vue'),
    },
    {
      path: '/news',
      name: 'news',
      component: () => import('./views/News.vue'),
    },
    {
      path: '/news/:id',
      name: 'news-single',
      component: () => import('./views/SingleNews.vue')
    },
    // masters
    {
      path: '/masters',
      name: 'masters',
      component: () => import('./views/spec/ItemList.vue'),
    },
    {
      path: '/masters/:slug',
      name: 'masters-single',
      component: () => import('./views/spec/Item.vue'),
    },
    {
      path: '/@:slug',
      name: 'masters-single-short-url',
      component: () => import('./views/spec/Item.vue'),
    },
    {
      path: '/masters/:slug/pricelist',
      name: 'master-pricelist',
      component: () => import('./views/spec/PriceList.vue'),
    },
    // companies
    {
      path: '/companies',
      name: 'companies',
      component: () => import('./views/org/ItemList.vue'),
    },
    {
      path: '/companies/:slug',
      name: 'companies-single',
      component: () => import('./views/org/Item.vue'),
    },
    {
      path: '/companies/:slug/invite-specialist',
      name: 'companies-invite-specialist',
      component: () => import('./views/org/SpecInvite.vue'),
    },
    {
      path: '/companies/:slug/admin',
      name: 'companies-single-admin',
      component: () => import('./views/org/Admin.vue'),
    },
    {
      path: '/companies/:slug/add-specialist',
      name: 'companies-add-specialist',
      component: () => import('./views/org/SpecCreate.vue'),
    },
    {
      path: '/companies/:slug/settings',
      name: 'company-settings',
      component: () => import('./views/org/Settings.vue')
    },
    {
      path: '/companies/:slug/schedule',
      name: 'companies-single-schedule',
      component: () => import('./views/org/SettingsSchedule.vue')
    },
    {
      path: '/companies/:slug/reservations',
      name: 'company-reservations',
      component: () => import('./views/org/Reservations.vue')
    },
    {
      path: '/companies/:slug/admin/specialists/:spec_slug',
      name: 'companies-single-admin-spec',
      component: () => import('./views/org/SpecCalendar.vue'),
    },
    {
      path: '/companies/:slug/admin/specialists/:spec_slug/schedule',
      name: 'companies-single-admin-spec-schedule',
      component: () => import('./views/org/SpecSettingsSchedule.vue'),
    },
    {
      path: '/companies/:slug/admin/specialists/:spec_slug/settings',
      name: 'companies-single-admin-spec-settings',
      component: () => import('./views/org/SpecSettings.vue'),
    },
    {
      path: '/user/reservations',
      name: 'user-reservations',
      component: () => import('./views/user/Reservations.vue')
    },
    {
      path: '/specialist/reservations',
      name: 'specialist-reservations',
      component: () => import('./views/spec/Reservations.vue')
    },
    {
      path: '/user/settings',
      name: 'user-settings',
      component: () => import('./views/user/Settings.vue')
    },
    {
      path: '/specialist/settings',
      name: 'spec-settings',
      component: () => import('./views/spec/Settings.vue')
    },
    {
      path: '/specialist/settings/schedule',
      name: 'spec-settings-schedule',
      component: () => import('./views/spec/SettingsSchedule.vue')
    },
    {
      path: '/specialist/calendar',
      name: 'masters-single-calendar',
      component: () => import('./views/spec/Calendar.vue'),
    },
    {
      path: '/user/become-a-specialist',
      name: 'become-spec',
      component: () => import('./views/spec/Create.vue')
    },
    {
      path: '/user/companies',
      name: 'user-companies',
      component: () => import('./views/user/Companies.vue')
    },
    {
      path: '/user/companies/create',
      name: 'user-companies-create',
      component: () => import('./views/org/Create.vue')
    },
    {
      path: '/search/:search_slug',
      name: 'search',
      component: () => import('./views/Search.vue')
    },
    {
      path: '/calendar',
      name: 'calendar',
      component: () => import('./components/Scheduler/layers/Test.vue')
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '*',
      name: 'error-404',
      component: () => import('./views/NotFound.vue'),
    }
  ]
}) 