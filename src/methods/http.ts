import axios from 'axios';

export default function(url: string, method: string, auth, payload): any {

    //const siteUrl = 'http://api.checkin.local/api/v1';
    const siteUrl = 'https://api.checkin.kg/api/v1';
    // const siteUrl = 'http://checkin.backend/api/v1'

    if (auth) {
        axios.defaults.headers.common['Authorization'] = `Token ${auth}`;
    }

    if (method === 'get') {
        return axios.get(`${siteUrl}/${url}/`, {
            params: payload
        });
    }

    if (method === 'post') {
        return axios.post(`${siteUrl}/${url}/`, payload);
    }

    if (method === 'delete') {
        return axios.delete(`${siteUrl}/${url}`, {
            data: payload
        });
    }

    if (method === 'put') {
        return axios.put(`${siteUrl}/${url}`, payload)
    }
}