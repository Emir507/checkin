// Import and configure the Firebase SDK
// These scripts are made available when the app is served or deployed on Firebase Hosting
// If you do not serve/host your project using Firebase Hosting see https://firebase.google.com/docs/web/setup
importScripts("https://www.gstatic.com/firebasejs/7.10.0/firebase-app.js");
importScripts(
  "https://www.gstatic.com/firebasejs/7.10.0/firebase-messaging.js"
);

/**
 * Here is is the code snippet to initialize Firebase Messaging in the Service
 * Worker when your app is not hosted on Firebase Hosting.
 // [START initialize_firebase_in_sw]
 // Give the service worker access to Firebase Messaging.
 // Note that you can only use Firebase Messaging here, other Firebase libraries
 // are not available in the service worker.
 importScripts('https://www.gstatic.com/firebasejs/7.10.0/firebase-app.js');
 importScripts('https://www.gstatic.com/firebasejs/7.10.0/firebase-messaging.js');
 // Initialize the Firebase app in the service worker by passing in the
 // messagingSenderId.
 firebase.initializeApp({
   'messagingSenderId': 'YOUR-SENDER-ID'
 });
 // Retrieve an instance of Firebase Messaging so that it can handle background
 // messages.
 const messaging = firebase.messaging();
 // [END initialize_firebase_in_sw]
 **/

firebase.initializeApp({
  messagingSenderId: "114594238374",
  apiKey: "AIzaSyAE0C5omlWMgf4aBJB3qKqygp6UhNTkzQs",
  authDomain: "checkin-notification-260ed.firebaseapp.com",
  databaseURL: "https://checkin-notification-260ed.firebaseio.com",
  projectId: "checkin-notification-260ed",
  storageBucket: "checkin-notification-260ed.appspot.com",
  appId: "1:114594238374:web:8b180f06f96a1a2fac0967",
  measurementId: "G-QEZB3C5Z27"
});
const messaging = firebase.messaging();
// If you would like to customize notifications that are received in the
// background (Web app is closed or not in browser focus) then you should
// implement this optional method.
// [START background_handler]
messaging.setBackgroundMessageHandler(function(payload) {
  console.log(
    "[firebase-messaging-sw.js] Received background message ",
    payload
  );
  // Customize notification here
  const notificationTitle = "Background Message Title";
  const notificationOptions = {
    body: "Background Message body.",
    icon: "/firebase-logo.png"
  };

  return self.registration.showNotification(
    notificationTitle,
    notificationOptions
  );
});
// [END background_handler]
